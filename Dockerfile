FROM openshift/jenkins:2
ENV OPENSHIFT_ENABLE_OAUTH true

USER root

COPY ./run-s2i-and-set-token /usr/libexec/s2i/run-s2i-and-set-token
COPY credentials.xml /opt/openshift/configuration/credentials.xml
RUN chmod a+x /usr/libexec/s2i/run-s2i-and-set-token

RUN sed -i 's@</authorizationStrategy>@<permission>hudson.model.Item.Build:authenticated</permission></authorizationStrategy>@g' /opt/openshift/configuration/config.xml.tpl && rm -rf /opt/openshift/configuration/jobs
COPY ./jobs/ /opt/openshift/configuration/jobs/

CMD /usr/libexec/s2i/run-s2i-and-set-token